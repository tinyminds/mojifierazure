"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Rect {
    constructor({ top, left, width, height }) {
        this.top = top;
        this.left = left;
        this.width = width;
        this.height = height;
    }
}
exports.Rect = Rect;
//# sourceMappingURL=rect.js.map