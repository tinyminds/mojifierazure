"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Jimp = require("jimp");
const path = require("path");
const FaceApi = require("../shared/faceapi");
function createMojifiedImage(context, imageUrl, faces) {
    return __awaiter(this, void 0, void 0, function* () {
        let sourceImage = yield Jimp.read(imageUrl);
        let compositeImage = sourceImage;
        if (faces.length == 0) {
            throw new Error(`No faces found in image`);
        }
        for (let face of faces) {
            const mojiName = face.mojiName;
            const faceHeight = face.faceRectangle.height;
            const faceWidth = face.faceRectangle.width;
            const faceTop = face.faceRectangle.top;
            const faceLeft = face.faceRectangle.left;
            let mojiPath = path.resolve(__dirname, "../shared/emojis/" + mojiName + ".png");
            let emojiImage = yield Jimp.read(mojiPath);
            emojiImage.resize(faceWidth, faceHeight);
            compositeImage = compositeImage.composite(emojiImage, faceLeft, faceTop);
        }
        try {
            return yield compositeImage.getBufferAsync(Jimp.MIME_JPEG);
        }
        catch (error) {
            context.log(`There was an error adding the emoji to the image: ${error}`);
            throw new Error(error);
        }
    });
}
function index(context, req) {
    return __awaiter(this, void 0, void 0, function* () {
        context.res = {
            status: 200,
            headers: {
                "Content-Type": "image/jpeg"
            },
            isRaw: true
        };
        const { imageUrl } = req.query;
        if (!imageUrl) {
            let message = `imageUrl is required`;
            context.log(message);
            context.res = { status: 400, body: message };
        }
        else {
            context.log(`Called with imageUrl: "${imageUrl}"`);
            try {
                let faces = yield FaceApi.getFaces(context, imageUrl);
                if (faces) {
                    let buffer = yield createMojifiedImage(context, imageUrl, faces);
                    context.res.body = buffer;
                    context.log(`Posted reply with mojified image`);
                }
                else {
                    context.res = { status: 400, body: `Could not process image: ${imageUrl}` };
                }
            }
            catch (err) {
                let message = `There was an error processing this image: ${err.message}`;
                context.log(message);
                context.res = { status: 400, body: message };
            }
        }
    });
}
exports.index = index;
//# sourceMappingURL=index.js.map